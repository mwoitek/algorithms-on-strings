#include <algorithm>
#include <array>
#include <iostream>
#include <sstream>
#include <unordered_map>
#include <vector>

using std::string;
using std::unordered_map;
using std::vector;

const std::array<char, 5> ALPHABET = {'$', 'A', 'C', 'G', 'T'};

struct LetterOccurr {
  char letter;
  unsigned occurrence;
  LetterOccurr(const char letter, const unsigned occurrence)
      : letter(letter), occurrence(occurrence) {}
};

// Defined so I can use std::find
bool operator==(const LetterOccurr& lhs, const LetterOccurr& rhs) {
  return lhs.letter == rhs.letter && lhs.occurrence == rhs.occurrence;
}

class BWInverter {
 public:
  string bw_transform;
  BWInverter(const string& bw_transform);
  string get_inverse();

 private:
  vector<LetterOccurr> first_col;
  vector<LetterOccurr> last_col;
  static unordered_map<char, unsigned> initialize_counter_map();
  static vector<LetterOccurr> count_occurrences(const vector<char>& col);
  void set_first_col();
  void set_last_col();
  size_t find_index(const LetterOccurr& lo);
};

BWInverter::BWInverter(const string& bw_transform)
    : bw_transform(bw_transform) {
  set_first_col();
  set_last_col();
}

string BWInverter::get_inverse() {
  std::stringstream reverse;
  reverse << '$';
  LetterOccurr lo = last_col.front();
  for (unsigned i = 1; i < bw_transform.length(); ++i) {
    const size_t idx = find_index(lo);
    reverse << first_col[idx].letter;
    lo = last_col[idx];
  }
  string bw_inverse(reverse.str());
  std::reverse(bw_inverse.begin(), bw_inverse.end());
  return bw_inverse;
}

unordered_map<char, unsigned> BWInverter::initialize_counter_map() {
  unordered_map<char, unsigned> counter_map;
  for (const char& letter : ALPHABET) {
    counter_map[letter] = 0;
  }
  return counter_map;
}

vector<LetterOccurr> BWInverter::count_occurrences(const vector<char>& col) {
  vector<LetterOccurr> col_occurr;
  unordered_map<char, unsigned> counter_map = initialize_counter_map();
  for (const char& letter : col) {
    col_occurr.emplace_back(letter, ++counter_map[letter]);
  }
  return col_occurr;
}

void BWInverter::set_first_col() {
  vector<char> col(bw_transform.cbegin(), bw_transform.cend());
  std::sort(col.begin(), col.end());
  first_col = count_occurrences(col);
}

void BWInverter::set_last_col() {
  const vector<char> col(bw_transform.cbegin(), bw_transform.cend());
  last_col = count_occurrences(col);
}

size_t BWInverter::find_index(const LetterOccurr& lo) {
  const auto it = std::find(first_col.cbegin(), first_col.cend(), lo);
  const auto dist = std::distance(first_col.cbegin(), it);
  return static_cast<size_t>(dist);
}

int main() {
  string bw_transform;
  std::cin >> bw_transform;
  BWInverter inverter = BWInverter(bw_transform);
  std::cout << inverter.get_inverse() << '\n';
  return 0;
}
