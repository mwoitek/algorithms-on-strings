#include <algorithm>
#include <array>
#include <iostream>
#include <limits>
#include <sstream>
#include <unordered_map>
#include <vector>

using std::cin;
using std::string;
using std::unordered_map;
using std::vector;

const std::array<char, 5> ALPHABET = {'$', 'A', 'C', 'G', 'T'};

// Reading input

struct Input {
  const vector<string> patterns;
  const string bw_transform;
  Input(const vector<string>& patterns, const string& bw_transform)
      : patterns(patterns), bw_transform(bw_transform) {}
};

vector<string> split_line(const string& line, const size_t num_parts,
                          const char separator = ' ') {
  vector<string> line_parts(num_parts);
  size_t i = 0;
  for (size_t j = 0; j < num_parts - 1; ++j) {
    const size_t sep_pos = line.find(separator, i);
    line_parts[j] = line.substr(i, sep_pos - i);
    i = sep_pos + 1;
  }
  line_parts[num_parts - 1] = line.substr(i);
  return line_parts;
}

Input read_input() {
  string bw_transform;
  cin >> bw_transform;

  size_t num_patterns;
  cin >> num_patterns;
  cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

  string line;
  std::getline(cin, line);
  const vector<string> patterns = split_line(line, num_patterns);

  return Input(patterns, bw_transform);
}

// Pattern counting

class LetterCounts {
 public:
  LetterCounts(const string& last_col) { count_letters(last_col); }

  unsigned long get_count(const char letter, const size_t idx) const {
    return counts.at(letter)[idx];
  }

 private:
  unordered_map<char, vector<unsigned long>> counts;

  void count_letters(const string& last_col) {
    unordered_map<char, unsigned long> counters;
    for (const char letter : ALPHABET) {
      counters[letter] = 0;
      counts[letter].push_back(0);
    }
    for (const char col_letter : last_col) {
      ++counters[col_letter];
      for (const char letter : ALPHABET) {
        counts[letter].push_back(counters[letter]);
      }
    }
  }
};

class PatternCounter {
 public:
  const vector<string> patterns;
  const string last_col;

  PatternCounter(const vector<string>& patterns, const string& last_col)
      : patterns(patterns),
        last_col(last_col),
        letter_counts(LetterCounts(last_col)) {
    set_first_column();
    set_first_occurr();
    count_patterns();
  }

  void print_counts() const {
    std::stringstream out_stream;
    for (const unsigned long cnt : pattern_counts) {
      out_stream << cnt << ' ';
    }
    string out = out_stream.str();
    out.pop_back();
    std::cout << out << '\n';
  }

 private:
  string first_col;
  unordered_map<char, size_t> first_occurr;
  LetterCounts letter_counts;
  vector<unsigned long> pattern_counts;

  void set_first_column() {
    vector<char> col_vec(last_col.cbegin(), last_col.cend());
    std::sort(col_vec.begin(), col_vec.end());
    const string fc(col_vec.begin(), col_vec.end());
    first_col = fc;
  }

  void set_first_occurr() {
    for (const char letter : ALPHABET) {
      first_occurr[letter] = first_col.find(letter);
    }
  }

  unsigned long count_pattern(const string& pattern) {
    unsigned long cnt = 0;
    size_t top = 0;
    size_t bottom = last_col.length() - 1;
    int pattern_idx = pattern.length() - 1;
    while (top <= bottom) {
      if (pattern_idx < 0) {
        cnt = bottom - top + 1;
        break;
      }
      const char letter = pattern.at(static_cast<size_t>(pattern_idx--));
      if (last_col.find(letter, top) > bottom) {
        break;
      }
      top = first_occurr.at(letter) + letter_counts.get_count(letter, top);
      bottom = first_occurr.at(letter) +
               letter_counts.get_count(letter, bottom + 1) - 1;
    }
    return cnt;
  }

  void count_patterns() {
    for (const string& pattern : patterns) {
      pattern_counts.push_back(count_pattern(pattern));
    }
  }
};

int main() {
  const Input input = read_input();
  const PatternCounter counter(input.patterns, input.bw_transform);
  counter.print_counts();
  return 0;
}
