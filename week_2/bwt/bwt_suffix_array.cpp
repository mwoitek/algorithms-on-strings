#include <array>
#include <iostream>
#include <sstream>
#include <unordered_map>
#include <vector>

using std::string;
using std::vector;

const unsigned ALPHABET_SIZE = 5;
const std::unordered_map<char, unsigned> CHAR_TO_IDX = {
    {'$', 0}, {'A', 1}, {'C', 2}, {'G', 3}, {'T', 4},
};

// Building the suffix array

auto sort_characters(const string& str) -> vector<unsigned> {
  vector<unsigned> order(str.length());
  std::array<unsigned, ALPHABET_SIZE> count{};
  for (const char& chr : str) {
    ++count.at(CHAR_TO_IDX.at(chr));
  }
  for (unsigned i = 1; i < ALPHABET_SIZE; ++i) {
    count.at(i) += count.at(i - 1);
  }
  for (unsigned i = str.length(); i > 0; --i) {
    const unsigned idx = CHAR_TO_IDX.at(str[i - 1]);
    --count.at(idx);
    order[count.at(idx)] = i - 1;
  }
  return order;
}

auto compute_char_classes(const string& str, const vector<unsigned>& order)
    -> vector<unsigned> {
  vector<unsigned> eq_class(str.length(), 0);
  for (unsigned i = 1; i < str.length(); ++i) {
    eq_class[order[i]] = str[order[i]] != str[order[i - 1]]
                             ? eq_class[order[i - 1]] + 1
                             : eq_class[order[i - 1]];
  }
  return eq_class;
}

auto sort_doubled(const string& str, const unsigned len,
                  const vector<unsigned>& order,
                  const vector<unsigned>& eq_class) -> vector<unsigned> {
  const unsigned str_len = str.length();
  vector<unsigned> new_order(str_len);
  vector<unsigned> count(str_len, 0);
  for (unsigned i = 0; i < str_len; ++i) {
    ++count[eq_class[i]];
  }
  for (unsigned i = 1; i < str_len; ++i) {
    count[i] += count[i - 1];
  }
  for (unsigned i = str_len; i > 0; --i) {
    const unsigned start = (order[i - 1] - len + str_len) % str_len;
    const unsigned clss = eq_class[start];
    --count[clss];
    new_order[count[clss]] = start;
  }
  return new_order;
}

auto update_classes(const vector<unsigned>& order,
                    const vector<unsigned>& eq_class, const unsigned len)
    -> vector<unsigned> {
  const unsigned str_len = order.size();
  vector<unsigned> new_eq_class(str_len, 0);
  for (unsigned i = 1; i < str_len; ++i) {
    const unsigned curr = order[i];
    const unsigned prev = order[i - 1];
    const unsigned mid = (curr + len) % str_len;
    const unsigned mid_prev = (prev + len) % str_len;
    new_eq_class[curr] =
        eq_class[curr] != eq_class[prev] || eq_class[mid] != eq_class[mid_prev]
            ? new_eq_class[prev] + 1
            : new_eq_class[prev];
  }
  return new_eq_class;
}

auto build_suffix_array(const string& str) -> vector<unsigned> {
  vector<unsigned> order = sort_characters(str);
  vector<unsigned> eq_class = compute_char_classes(str, order);
  for (unsigned len = 1; len < str.length(); len *= 2) {
    order = sort_doubled(str, len, order, eq_class);
    eq_class = update_classes(order, eq_class, len);
  }
  return order;
}

// Computing the Burrows-Wheeler transform from the suffix array

auto burrows_wheeler_transform(const string& input_string) -> string {
  const vector<unsigned> suffix_array = build_suffix_array(input_string);
  std::stringstream transform;
  for (unsigned i = 0; i < input_string.length(); ++i) {
    const char symbol =
        suffix_array[i] > 0 ? input_string[suffix_array[i] - 1] : '$';
    transform << symbol;
  }
  return transform.str();
}

auto main() -> int {
  string input_string;
  std::cin >> input_string;
  std::cout << burrows_wheeler_transform(input_string) << '\n';
  return 0;
}
