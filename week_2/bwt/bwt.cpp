#include <algorithm>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using std::string;
using std::vector;

// Naive implementation of Burrows-Wheeler transform

string next_shifted_string(const string& last_string) {
  string shifted_string = "";
  shifted_string.push_back(last_string.back());
  shifted_string += last_string.substr(0, last_string.length() - 1);
  return shifted_string;
}

vector<string> get_shifted_strings(const string& input_string) {
  vector<string> shifted_strings(input_string.length());
  shifted_strings[0] = input_string;
  for (size_t i = 1; i < shifted_strings.size(); ++i) {
    shifted_strings[i] = next_shifted_string(shifted_strings[i - 1]);
  }
  std::sort(shifted_strings.begin(), shifted_strings.end());
  return shifted_strings;
}

string join_last_column(const vector<string>& shifted_strings) {
  std::stringstream joined;
  for (const string& shifted_string : shifted_strings) {
    joined << shifted_string.back();
  }
  return joined.str();
}

string burrows_wheeler_transform(const string& input_string) {
  const vector<string> shifted_strings = get_shifted_strings(input_string);
  return join_last_column(shifted_strings);
}

int main() {
  string input_string;
  std::cin >> input_string;
  std::cout << burrows_wheeler_transform(input_string) << '\n';
  return 0;
}
