#include <algorithm>
#include <iostream>
#include <numeric>
#include <sstream>
#include <vector>

using std::string;
using std::vector;

// Generate all suffixes
vector<string> generate_suffixes(const string& input_string) {
  const size_t input_length = input_string.length();
  vector<string> suffixes(input_length);
  for (size_t i = 0; i < input_length; ++i) {
    suffixes[i] = input_string.substr(i);
  }
  return suffixes;
}

// Create suffix array
vector<size_t> create_suffix_array(const string& input_string) {
  vector<size_t> suffix_array(input_string.length());
  std::iota(suffix_array.begin(), suffix_array.end(), 0);
  const vector<string> suffixes = generate_suffixes(input_string);
  const auto comp_func = [&suffixes](const size_t i, const size_t j) {
    return suffixes[i] < suffixes[j];
  };
  std::stable_sort(suffix_array.begin(), suffix_array.end(), comp_func);
  return suffix_array;
}

// Print suffix array
void print_suffix_array(const vector<size_t>& suffix_array) {
  std::stringstream out_stream;
  for (const size_t index : suffix_array) {
    out_stream << index << ' ';
  }
  string out = out_stream.str();
  out.pop_back();
  std::cout << out << '\n';
}

int main() {
  string input_string;
  std::cin >> input_string;
  const vector<size_t> suffix_array = create_suffix_array(input_string);
  print_suffix_array(suffix_array);
  return 0;
}
