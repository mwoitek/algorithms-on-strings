#include <array>
#include <iostream>
#include <memory>
#include <stack>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::string;
using std::unique_ptr;
using std::vector;

const size_t ALPHABET_SIZE = 4;

// Function for reading input
auto read_input() -> vector<string> {
  size_t num_patterns = 0;
  cin >> num_patterns;
  vector<string> patterns(num_patterns);
  for (size_t i = 0; i < num_patterns; ++i) {
    cin >> patterns[i];
  }
  return patterns;
}

// Node of trie data structure
struct TrieNode {
  std::array<unique_ptr<TrieNode>, ALPHABET_SIZE> children{};
  int label;
  TrieNode(const int label) : label(label) {}
};

// Function for converting a letter into an array index
auto letter_to_index(const char& letter) -> size_t {
  switch (letter) {
    case 'A':
      return 0;
    case 'C':
      return 1;
    case 'G':
      return 2;
    default:
      return 3;
  }
}

// Function for converting an array index into a letter
auto index_to_letter(const size_t index) -> char {
  switch (index) {
    case 0:
      return 'A';
    case 1:
      return 'C';
    case 2:
      return 'G';
    default:
      return 'T';
  }
}

// Incomplete implementation of a trie.
// Only what's needed to solve this problem.
struct Trie {
  unique_ptr<TrieNode> root;
  int curr_label{1};

  Trie() : root(std::make_unique<TrieNode>(0)) {}

  void insert(const string& pattern) {
    TrieNode* curr_node = root.get();
    for (const char& letter : pattern) {
      const size_t idx = letter_to_index(letter);
      if (curr_node->children.at(idx) == nullptr) {
        curr_node->children.at(idx) = std::make_unique<TrieNode>(curr_label++);
      }
      curr_node = curr_node->children.at(idx).get();
    }
  }

  void insert_many(const vector<string>& patterns) {
    for (const string& pattern : patterns) {
      insert(pattern);
    }
  }
};

void print_adjacency_list(const Trie& trie) {
  std::stack<const TrieNode*> stck;
  stck.push(trie.root.get());
  while (!stck.empty()) {
    const TrieNode* const curr_node = stck.top();
    stck.pop();
    for (size_t i = 0; i < ALPHABET_SIZE; ++i) {
      if (curr_node->children.at(i) == nullptr) {
        continue;
      }
      cout << curr_node->label << "->" << curr_node->children.at(i)->label;
      cout << ':' << index_to_letter(i) << '\n';
      stck.push(curr_node->children.at(i).get());
    }
  }
}

auto main() -> int {
  const vector<string> patterns = read_input();
  Trie trie = Trie();
  trie.insert_many(patterns);
  print_adjacency_list(trie);
  return 0;
}
