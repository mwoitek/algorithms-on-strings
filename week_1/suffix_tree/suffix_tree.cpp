#include <iostream>
#include <vector>

using std::string;
using std::vector;

// Struct for storing the first index and the length of a substring
struct Substr {
  size_t first;
  size_t length;
  Substr(const size_t first, const size_t length)
      : first(first), length(length) {}
};

// Node of a suffix tree
struct Node {
  Substr substr;
  vector<size_t> children;

  Node(const size_t first = 0, const size_t length = 0)
      : substr(Substr(first, length)) {}

  Node(const size_t first, const size_t length, const size_t child)
      : substr(Substr(first, length)) {
    add_child(child);
  }

  void add_child(const size_t child) { children.push_back(child); }

  size_t get_num_children() { return children.size(); }
};

// Naive implementation of a suffix tree
// Strings are not stored in the edges but in the nodes!
struct SuffixTree {
  string str;
  vector<Node> nodes;

  SuffixTree(const string& str) : str(str) {
    add_node();
    for (size_t suffix_start = 0; suffix_start < str.length(); ++suffix_start) {
      add_suffix(suffix_start);
    }
  }

  void add_node(const size_t first = 0, const size_t length = 0) {
    nodes.push_back(Node(first, length));
  }

  void add_node(const size_t first, const size_t length, const size_t child) {
    nodes.push_back(Node(first, length, child));
  }

  size_t get_num_nodes() { return nodes.size(); }

  void add_child(const size_t parent, const size_t child) {
    nodes[parent].add_child(child);
  }

  size_t get_child(const size_t parent, const size_t child) {
    return nodes[parent].children[child];
  }

  size_t get_num_children(const size_t idx) {
    return nodes[idx].get_num_children();
  }

  size_t get_suffix_length(const size_t start) { return str.length() - start; }

  char get_letter(const size_t start, const size_t idx) {
    return str.at(start + idx);
  }

  char get_substr_first_letter(const size_t idx) {
    return str.at(nodes[idx].substr.first);
  }

  void add_suffix(const size_t suffix_start) {
    const size_t suffix_length = get_suffix_length(suffix_start);
    size_t i = 0;
    size_t n1 = 0;

    while (i < suffix_length) {
      const char letter = get_letter(suffix_start, i);
      const size_t num_children = get_num_children(n1);
      size_t c = 0;
      size_t n2;

      while (true) {
        if (c == num_children) {
          n2 = get_num_nodes();
          add_node(suffix_start + i, suffix_length - i);
          add_child(n1, n2);
          return;
        }

        n2 = get_child(n1, c);
        if (get_substr_first_letter(n2) == letter) {
          break;
        }
        ++c;
      }

      size_t j = 0;
      const Substr substr = nodes[n2].substr;

      while (j < substr.length) {
        if (get_letter(substr.first, j) == get_letter(suffix_start, i + j)) {
          ++j;
          continue;
        }

        const size_t n3 = n2;
        n2 = get_num_nodes();
        add_node(substr.first, j, n3);
        nodes[n3].substr = Substr(substr.first + j, substr.length - j);
        nodes[n1].children[c] = n2;
        break;
      }

      i += j;
      n1 = n2;
    }
  }

  string get_substring(const Substr& substr) {
    return str.substr(substr.first, substr.length);
  }

  void print_strings() {
    for (size_t i = 1; i < get_num_nodes(); ++i) {
      std::cout << get_substring(nodes[i].substr) << '\n';
    }
  }
};

int main() {
  string input_string;
  std::cin >> input_string;
  SuffixTree st = SuffixTree(input_string);
  st.print_strings();
  return 0;
}
