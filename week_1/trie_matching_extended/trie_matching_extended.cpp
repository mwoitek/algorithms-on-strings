#include <array>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>

using std::cin;
using std::string;
using std::unique_ptr;
using std::vector;

const size_t ALPHABET_SIZE = 4;
const std::unordered_map<char, size_t> CHAR_TO_IDX = {
    {'A', 0},
    {'C', 1},
    {'G', 2},
    {'T', 3},
};

// Reading input

struct Input {
  string text;
  vector<string> patterns;
};

auto read_input() -> Input {
  string text;
  cin >> text;
  size_t num_patterns = 0;
  cin >> num_patterns;
  vector<string> patterns(num_patterns);
  for (size_t i = 0; i < num_patterns; ++i) {
    cin >> patterns[i];
  }
  return {text, patterns};
}

// Node of trie data structure
struct TrieNode {
  std::array<unique_ptr<TrieNode>, ALPHABET_SIZE> children{};
  bool is_end{};
};

// Incomplete implementation of trie.
// Only what's needed to solve this problem.
struct Trie {
  unique_ptr<TrieNode> root;

  Trie() : root(std::make_unique<TrieNode>()) {}

  void insert(const string& pattern) const {
    TrieNode* curr_node = root.get();
    for (const char& letter : pattern) {
      const size_t idx = CHAR_TO_IDX.at(letter);
      if (curr_node->children.at(idx) == nullptr) {
        curr_node->children.at(idx) = std::make_unique<TrieNode>();
      }
      curr_node = curr_node->children.at(idx).get();
    }
    curr_node->is_end = true;
  }

  void insert_many(const vector<string>& patterns) const {
    for (const string& pattern : patterns) {
      insert(pattern);
    }
  }
};

// Trie matching

void update_indexes(vector<size_t>& indexes, const string& text,
                    const size_t start, const Trie& trie) {
  size_t text_idx = start;
  TrieNode* curr_node = trie.root.get();
  while (true) {
    if (curr_node->is_end) {
      indexes.push_back(start);
      return;
    }
    if (text_idx == text.length()) {
      return;
    }
    const size_t char_idx = CHAR_TO_IDX.at(text[text_idx]);
    if (curr_node->children.at(char_idx) != nullptr) {
      ++text_idx;
      curr_node = curr_node->children.at(char_idx).get();
    } else {
      return;
    }
  }
}

auto get_indexes(const string& text, const Trie& trie) -> vector<size_t> {
  vector<size_t> indexes;
  for (size_t start = 0; start < text.length(); ++start) {
    update_indexes(indexes, text, start, trie);
  }
  return indexes;
}

void print_indexes(const vector<size_t>& indexes) {
  std::stringstream out_stream;
  for (const size_t index : indexes) {
    out_stream << index << ' ';
  }
  string out = out_stream.str();
  if (out.length() > 0) {
    out.pop_back();
  }
  std::cout << out << '\n';
}

auto main() -> int {
  const Input input = read_input();

  const Trie trie = Trie();
  trie.insert_many(input.patterns);

  const vector<size_t> indexes = get_indexes(input.text, trie);
  print_indexes(indexes);

  return 0;
}
