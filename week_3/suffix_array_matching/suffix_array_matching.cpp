#include <array>
#include <cmath>
#include <iostream>
#include <limits>
#include <sstream>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using std::cin;
using std::string;
using std::unordered_set;
using std::vector;

const unsigned ALPHABET_SIZE = 5;
const std::unordered_map<char, unsigned> CHAR_TO_IDX = {
    {'$', 0}, {'A', 1}, {'C', 2}, {'G', 3}, {'T', 4},
};

// Reading input

struct Input {
  string text;
  vector<string> patterns;
};

auto split_line(const string& line, const unsigned num_parts,
                const char separator = ' ') -> vector<string> {
  vector<string> line_parts(num_parts);
  unsigned pos = 0;
  for (unsigned i = 0; i < num_parts - 1; ++i) {
    const unsigned sep_pos = line.find(separator, pos);
    line_parts[i] = line.substr(pos, sep_pos - pos);
    pos = sep_pos + 1;
  }
  line_parts[num_parts - 1] = line.substr(pos);
  return line_parts;
}

auto read_input() -> Input {
  Input input;

  cin >> input.text;
  input.text.push_back('$');

  unsigned num_patterns;
  cin >> num_patterns;
  cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

  string line;
  std::getline(cin, line);
  input.patterns = split_line(line, num_patterns);

  return input;
}

// Building the suffix array

auto sort_characters(const string& str) -> vector<unsigned> {
  vector<unsigned> order(str.length());
  std::array<unsigned, ALPHABET_SIZE> count{};
  for (const char& chr : str) {
    ++count[CHAR_TO_IDX.at(chr)];
  }
  for (unsigned i = 1; i < ALPHABET_SIZE; ++i) {
    count[i] += count[i - 1];
  }
  for (unsigned i = str.length(); i > 0; --i) {
    const unsigned idx = CHAR_TO_IDX.at(str[i - 1]);
    --count[idx];
    order[count[idx]] = i - 1;
  }
  return order;
}

auto compute_char_classes(const string& str, const vector<unsigned>& order)
    -> vector<unsigned> {
  vector<unsigned> eq_class(str.length(), 0);
  for (unsigned i = 1; i < str.length(); ++i) {
    eq_class[order[i]] = str[order[i]] != str[order[i - 1]]
                             ? eq_class[order[i - 1]] + 1
                             : eq_class[order[i - 1]];
  }
  return eq_class;
}

auto sort_doubled(const string& str, const unsigned len,
                  const vector<unsigned>& order,
                  const vector<unsigned>& eq_class) -> vector<unsigned> {
  const unsigned str_len = str.length();
  vector<unsigned> new_order(str_len);
  vector<unsigned> count(str_len, 0);
  for (unsigned i = 0; i < str_len; ++i) {
    ++count[eq_class[i]];
  }
  for (unsigned i = 1; i < str_len; ++i) {
    count[i] += count[i - 1];
  }
  for (unsigned i = str_len; i > 0; --i) {
    const unsigned start = (order[i - 1] - len + str_len) % str_len;
    const unsigned clss = eq_class[start];
    --count[clss];
    new_order[count[clss]] = start;
  }
  return new_order;
}

auto update_classes(const vector<unsigned>& order,
                    const vector<unsigned>& eq_class, const unsigned len)
    -> vector<unsigned> {
  const unsigned str_len = order.size();
  vector<unsigned> new_eq_class(str_len, 0);
  for (unsigned i = 1; i < str_len; ++i) {
    const unsigned curr = order[i];
    const unsigned prev = order[i - 1];
    const unsigned mid = (curr + len) % str_len;
    const unsigned mid_prev = (prev + len) % str_len;
    new_eq_class[curr] =
        eq_class[curr] != eq_class[prev] || eq_class[mid] != eq_class[mid_prev]
            ? new_eq_class[prev] + 1
            : new_eq_class[prev];
  }
  return new_eq_class;
}

auto build_suffix_array(const string& str) -> vector<unsigned> {
  vector<unsigned> order = sort_characters(str);
  vector<unsigned> eq_class = compute_char_classes(str, order);
  for (unsigned len = 1; len < str.length(); len *= 2) {
    order = sort_doubled(str, len, order, eq_class);
    eq_class = update_classes(order, eq_class, len);
  }
  return order;
}

// Pattern matching using the suffix array

// https://en.wikipedia.org/wiki/Suffix_array#Applications
// https://www.youtube.com/watch?v=jFewnXFVsPc

struct Interval {
  unsigned start;
  unsigned end;
  bool is_valid;
};

auto midpoint(const unsigned low, const unsigned high) -> unsigned {
  const double mean = static_cast<double>(low + high) / 2;
  return static_cast<unsigned>(std::floor(mean));
}

auto find_pattern(const string& pattern, const string& text,
                  const vector<unsigned>& suffix_array) -> Interval {
  const unsigned pat_len = pattern.length();
  const unsigned txt_len = text.length();

  unsigned low = 0;
  unsigned high = txt_len;
  while (low < high) {
    const unsigned mid = midpoint(low, high);
    if (text.compare(suffix_array[mid], pat_len, pattern) < 0) {
      low = mid + 1;
    } else {
      high = mid;
    }
  }

  if (text.compare(suffix_array[low], pat_len, pattern) != 0) {
    return {0, 0, false};
  }
  Interval interval{low, 0, true};

  high = txt_len;
  while (low < high) {
    const unsigned mid = midpoint(low, high);
    if (text.compare(suffix_array[mid], pat_len, pattern) > 0) {
      high = mid;
    } else {
      low = mid + 1;
    }
  }

  interval.end = high - 1;
  return interval;
}

auto find_patterns(const vector<string>& patterns, const string& text,
                   const vector<unsigned>& suffix_array)
    -> unordered_set<unsigned> {
  unordered_set<unsigned> indexes;
  for (const string& pattern : patterns) {
    const Interval interval = find_pattern(pattern, text, suffix_array);
    if (!interval.is_valid) {
      continue;
    }
    for (unsigned i = interval.start; i <= interval.end; ++i) {
      indexes.insert(suffix_array[i]);
    }
  }
  return indexes;
}

void print_indexes(const unordered_set<unsigned>& indexes) {
  std::stringstream out_stream;
  for (const unsigned index : indexes) {
    out_stream << index << ' ';
  }
  string out = out_stream.str();
  if (out.length() > 0) {
    out.pop_back();
  }
  std::cout << out << '\n';
}

auto main() -> int {
  const Input input = read_input();
  const vector<unsigned> suffix_array = build_suffix_array(input.text);
  const unordered_set<unsigned> indexes =
      find_patterns(input.patterns, input.text, suffix_array);
  print_indexes(indexes);
  return 0;
}
