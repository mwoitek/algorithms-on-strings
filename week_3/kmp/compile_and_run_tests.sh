#!/bin/bash
base_name="kmp"
num_test_cases=3
g++ -std=c++14 -O2 -Wall -Werror -Wextra -Wsign-conversion -pedantic "$base_name".cpp -o "$base_name".out || exit 1
for ((i = 1; i <= "$num_test_cases"; i++)); do
  echo "Test case ${i}"
  ./"$base_name".out <test_case_"$i".txt || exit 1
done
trash-put "$base_name".out
