#include <iostream>
#include <sstream>
#include <vector>

using std::string;
using std::vector;

// Computing the prefix function
auto compute_prefix_function(const string& pattern) -> vector<unsigned> {
  vector<unsigned> prefix_function(pattern.length(), 0);
  unsigned border = 0;
  for (size_t i = 1; i < pattern.length(); ++i) {
    while (border > 0 && pattern[i] != pattern[border]) {
      border = prefix_function[border - 1];
    }
    border = pattern[i] == pattern[border] ? border + 1 : 0;
    prefix_function[i] = border;
  }
  return prefix_function;
}

// Implementation of Knuth-Morris-Pratt algorithm
auto find_all_occurrences(const string& pattern, const string& text)
    -> vector<size_t> {
  vector<size_t> indexes;
  const string combined = pattern + "$" + text;
  const vector<unsigned> prefix_function = compute_prefix_function(combined);
  const size_t pattern_len = pattern.length();
  const size_t double_pattern_len = 2 * pattern_len;
  for (size_t i = pattern_len + 1; i < combined.length(); ++i) {
    if (prefix_function[i] == pattern_len) {
      indexes.push_back(i - double_pattern_len);
    }
  }
  return indexes;
}

void print_indexes(const vector<size_t>& indexes) {
  std::stringstream out_stream;
  for (const size_t index : indexes) {
    out_stream << index << ' ';
  }
  string out = out_stream.str();
  if (out.length() > 0) {
    out.pop_back();
  }
  std::cout << out << '\n';
}

auto main() -> int {
  string pattern;
  std::cin >> pattern;
  string genome;
  std::cin >> genome;
  const vector<size_t> indexes = find_all_occurrences(pattern, genome);
  print_indexes(indexes);
  return 0;
}
