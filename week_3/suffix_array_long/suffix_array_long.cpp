#include <array>
#include <iostream>
#include <sstream>
#include <unordered_map>
#include <vector>

using std::string;
using std::vector;

const size_t ALPHABET_SIZE = 5;
const std::unordered_map<char, size_t> CHAR_TO_IDX = {
    {'$', 0}, {'A', 1}, {'C', 2}, {'G', 3}, {'T', 4},
};

// Implementation of SortCharacters (Counting sort)
auto sort_characters(const string& str) -> vector<size_t> {
  vector<size_t> order(str.length());
  std::array<size_t, ALPHABET_SIZE> count{};
  for (const char& chr : str) {
    ++count[CHAR_TO_IDX.at(chr)];
  }
  for (size_t i = 1; i < ALPHABET_SIZE; ++i) {
    count[i] += count[i - 1];
  }
  for (size_t i = str.length(); i > 0; --i) {
    const size_t idx = CHAR_TO_IDX.at(str[i - 1]);
    --count[idx];
    order[count[idx]] = i - 1;
  }
  return order;
}

// Implementation of ComputeCharClasses
auto compute_char_classes(const string& str, const vector<size_t>& order)
    -> vector<size_t> {
  vector<size_t> eq_class(str.length(), 0);
  for (size_t i = 1; i < str.length(); ++i) {
    eq_class[order[i]] = str[order[i]] != str[order[i - 1]]
                             ? eq_class[order[i - 1]] + 1
                             : eq_class[order[i - 1]];
  }
  return eq_class;
}

// Implementation of SortDoubled (Counting sort again!)
auto sort_doubled(const string& str, const size_t len,
                  const vector<size_t>& order, const vector<size_t>& eq_class)
    -> vector<size_t> {
  vector<size_t> new_order(str.length());
  vector<size_t> count(str.length(), 0);
  for (size_t i = 0; i < str.length(); ++i) {
    ++count[eq_class[i]];
  }
  for (size_t i = 1; i < str.length(); ++i) {
    count[i] += count[i - 1];
  }
  for (size_t i = str.length(); i > 0; --i) {
    const size_t start = (order[i - 1] - len + str.length()) % str.length();
    const size_t clss = eq_class[start];
    --count[clss];
    new_order[count[clss]] = start;
  }
  return new_order;
}

// Implementation of UpdateClasses
auto update_classes(const vector<size_t>& order, const vector<size_t>& eq_class,
                    const size_t len) -> vector<size_t> {
  const size_t str_len = order.size();
  vector<size_t> new_eq_class(str_len, 0);
  for (size_t i = 1; i < str_len; ++i) {
    const size_t curr = order[i];
    const size_t prev = order[i - 1];
    const size_t mid = (curr + len) % str_len;
    const size_t mid_prev = (prev + len) % str_len;
    new_eq_class[curr] =
        eq_class[curr] != eq_class[prev] || eq_class[mid] != eq_class[mid_prev]
            ? new_eq_class[prev] + 1
            : new_eq_class[prev];
  }
  return new_eq_class;
}

// Implementation of BuildSuffixArray
auto build_suffix_array(const string& str) -> vector<size_t> {
  vector<size_t> order = sort_characters(str);
  vector<size_t> eq_class = compute_char_classes(str, order);
  for (size_t len = 1; len < str.length(); len *= 2) {
    order = sort_doubled(str, len, order, eq_class);
    eq_class = update_classes(order, eq_class, len);
  }
  return order;
}

void print_suffix_array(const vector<size_t>& suffix_array) {
  std::stringstream out_stream;
  for (const size_t index : suffix_array) {
    out_stream << index << ' ';
  }
  string out = out_stream.str();
  out.pop_back();
  std::cout << out << '\n';
}

auto main() -> int {
  string input_str;
  std::cin >> input_str;
  const vector<size_t> suffix_array = build_suffix_array(input_str);
  print_suffix_array(suffix_array);
  return 0;
}
