#include <iostream>
#include <limits>
#include <map>
#include <memory>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using std::cin;
using std::string;
using std::unique_ptr;
using std::vector;

// Reading input

struct Input {
  string text;
  vector<ulong> suffix_array;
  vector<ulong> lcp_array;
};

auto split_line(const string& line, const ulong num_parts,
                const char separator = ' ') -> vector<ulong> {
  vector<ulong> line_parts(num_parts);
  ulong pos = 0;
  for (ulong i = 0; i < num_parts - 1; ++i) {
    const ulong sep_pos = line.find(separator, pos);
    line_parts[i] = std::stoul(line.substr(pos, sep_pos - pos));
    pos = sep_pos + 1;
  }
  line_parts[num_parts - 1] = std::stoul(line.substr(pos));
  return line_parts;
}

auto read_input() -> Input {
  Input input;

  cin >> input.text;
  cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

  string line;
  std::getline(cin, line);
  input.suffix_array = split_line(line, input.text.length());

  std::getline(cin, line);
  input.lcp_array = split_line(line, input.text.length() - 1);

  return input;
}

// Building the suffix tree

struct Edge {
  ulong start;
  ulong end;
};

struct SuffixTreeNode {
  SuffixTreeNode* parent{nullptr};
  std::map<char, unique_ptr<SuffixTreeNode>> children;
  Edge edge{};
  ulong string_depth{};

  SuffixTreeNode() = default;

  SuffixTreeNode(SuffixTreeNode* const parent, const Edge& edge,
                 const ulong string_depth)
      : parent(parent), edge(edge), string_depth(string_depth){};
};

auto create_new_leaf(SuffixTreeNode* const node, const string& text,
                     const ulong suffix) -> SuffixTreeNode* {
  const Edge edge{node->string_depth + suffix, text.length() - 1};
  const ulong string_depth = text.length() - suffix;
  node->children[text[edge.start]] =
      std::make_unique<SuffixTreeNode>(node, edge, string_depth);
  return node->children[text[edge.start]].get();
}

auto break_edge(SuffixTreeNode* const node, const string& text,
                const ulong start, const ulong offset) -> SuffixTreeNode* {
  const Edge edge{start, start + offset - 1};
  const ulong string_depth = node->string_depth + offset;
  unique_ptr<SuffixTreeNode> mid_node =
      std::make_unique<SuffixTreeNode>(node, edge, string_depth);

  const char start_char = text[start];
  const char mid_char = text[start + offset];

  mid_node->children[mid_char] = std::move(node->children[start_char]);
  mid_node->children[mid_char]->parent = mid_node.get();
  mid_node->children[mid_char]->edge.start += offset;
  node->children[start_char] = std::move(mid_node);

  return node->children[start_char].get();
}

auto tree_from_array(const string& text, const vector<ulong>& suffix_array,
                     const vector<ulong>& lcp_array)
    -> unique_ptr<SuffixTreeNode> {
  unique_ptr<SuffixTreeNode> root = std::make_unique<SuffixTreeNode>();

  SuffixTreeNode* curr_node = root.get();
  ulong lcp_prev = 0;

  for (ulong i = 0; i < text.length(); ++i) {
    const ulong suffix = suffix_array[i];

    while (curr_node->string_depth > lcp_prev) {
      curr_node = curr_node->parent;
    }

    if (curr_node->string_depth == lcp_prev) {
      curr_node = create_new_leaf(curr_node, text, suffix);
    } else {
      const ulong edge_start = curr_node->string_depth + suffix_array[i - 1];
      const ulong offset = lcp_prev - curr_node->string_depth;
      SuffixTreeNode* mid_node =
          break_edge(curr_node, text, edge_start, offset);
      curr_node = create_new_leaf(mid_node, text, suffix);
    }

    if (i < text.length() - 1) {
      lcp_prev = lcp_array[i];
    }
  }

  return root;
}

// Printing the edges of the suffix tree

void print_edges(const SuffixTreeNode* const root) {
  if (root == nullptr) {
    return;
  }

  std::stack<const SuffixTreeNode*> node_stack;
  for (auto it = root->children.crbegin(); it != root->children.crend(); ++it) {
    node_stack.push(it->second.get());
  }

  while (!node_stack.empty()) {
    const SuffixTreeNode* const curr_node = node_stack.top();
    node_stack.pop();

    std::cout << curr_node->edge.start << ' ' << curr_node->edge.end + 1
              << '\n';

    for (auto it = curr_node->children.crbegin();
         it != curr_node->children.crend(); ++it) {
      node_stack.push(it->second.get());
    }
  }
}

auto main() -> int {
  const Input input = read_input();
  std::cout << input.text << '\n';

  const unique_ptr<const SuffixTreeNode> root =
      tree_from_array(input.text, input.suffix_array, input.lcp_array);
  print_edges(root.get());

  return 0;
}
